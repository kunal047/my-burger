import axios from 'axios';

const instance = axios.create({
    baseURL: "https://my-burger-app-b09ac.firebaseio.com/"
});

export default instance;

